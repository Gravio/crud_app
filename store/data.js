
export const actions = {
    saveData({commit}, formData) {
      try {

        if (localStorage.getItem('crudapp') === null) {

          const data = []
          data.push(formData)

          localStorage.setItem('crudapp', JSON.stringify(data)) 

        }else {

          const data = JSON.parse(localStorage.getItem('crudapp'))
          data.push(formData)

          localStorage.setItem('crudapp', JSON.stringify(data)) 
        }

      } catch (e) {
        commit('setError', e, {root: true})
        throw e
      }
    }
}